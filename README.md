# gitlab-terraform-gke-argocd
The purpose of this repository is to spin up a GKE cluster with ArgoCD pre-installed so you can configure your cluster through GitOps

## Prequisities

- kubectl client v1.23.9
- kubectl server v1.23.16-gke.1400
- A GCP Account that can provision a GKE Cluster
- working knowledge of GKE
- basic knowledge of ArgoCD

## Getting started

1. Clone this project and setup your CI/CD variables with `BASE64_GOOGLE_CREDENTIALS` and `TF_VAR_gcp_project` then run the GitLab CI pipeline. (Optional: You can change the name of this cluster in variables.tf)

![GCP CICD Variables](gke-cicd-variables.png){width=50%}


2. Connect to your GKE Cluster through your terminal using the following bash command
```bash
gcloud container clusters get-credentials gitlab-terraform-gke-argocd --region us-central1 --project demosys-cs-vendors-e0d84768
```

3. Expose the Initial Admin Secret through your terminal using the following bash command. Make sure you save this password for later
```bash
kubectl -n argocd get secret argocd-initial-admin-secret -o jsonpath="{.data.password}" | base64 -d
```

4. Port Forward ArgoCD to your localhost 8080 through your terminal using the following bash command. Go to Chrome localhost:8080 afterwards
```bash
kubectl port-forward svc/argocd-server -n argocd 8080:443
```

5. Enter your admin and `Initial Admin Secret` to the Login Page

![ArgoCD Login page](argocd-login.png){width=50%}


## 💥 Voila! You've bootstrapped your GKE cluster with ArgoCD. Enjoy your GitOps!

![ArgoCD Home page](argocd-home.png){width=50%}

### For more details on how to utilize ArgoCD, please read https://argo-cd.readthedocs.io/en/release-2.0/getting_started/
